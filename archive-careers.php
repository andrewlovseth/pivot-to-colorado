<?php get_header(); ?>

	<?php if(get_field('careers_google_tags', 'options')): ?>
		<?php the_field('careers_google_tags', 'options'); ?>
	<?php endif; ?>

	<section id="hero">

		<div class="wrapper">

			<div class="svg-header">
				<div class="desktop">
					<img src="<?php $image = get_field('careers_svg_header', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="mobile">
					<h1><?php the_field('featured_careers_headline', 'options'); ?></h1>
				</div>
			</div>			

			<div class="deck">
				<?php the_field('featured_careers_deck', 'options'); ?>
			</div>

			<div class="tabs">
				<a href="#" data-cat="" class="all active">All <br/>Careers</a>
				<a href="#" data-cat="engineering-technology">Engineering &amp; <br/>Technology</a>
				<a href="#" data-cat="product-design">Product &amp; <br/>Design</a>
				<a href="#" data-cat="data-analytics">Data &amp; <br/>Analytics</a>
				<a href="#" data-cat="sales-marketing">Sales &amp; <br/>Marketing</a>
				<a href="#" data-cat="other">Other</a>
			</div>

		</div>
	</section>


	<section id="filters">
		<div class="wrapper">
			<h5>Filter by company:</h5>

			<?php $posts = get_field('companies_list', 'options'); if( $posts ): ?>

				<div class="companies-dropdown">
					<div class="selected">
						<div class="item">
							<span></span>
						</div>
					</div>

					<div class="list">
						<div class="item">
							<a href="#" data-company="" class="all active">All Companies</a>
						</div>

					    <?php foreach( $posts as $post): setup_postdata($post); ?>

							<div class="item">
								<a href="#" data-company="<?php echo get_the_ID(); ?>"><?php the_title(); ?></a>
							</div>

					    <?php endforeach; ?>
					</div>

				</div>
			
			<?php wp_reset_postdata(); endif; ?>

		</div>
	</section>


	<section id="results">
		<div class="wrapper">

			<h5>Results</h5>

			<section id="posts">

				<section id="response">

				</section>

			</section>

		</div>
	</section>

	
<?php get_footer(); ?>