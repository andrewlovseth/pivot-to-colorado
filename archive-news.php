<?php get_header(); ?>

	<section id="hero">
		<div class="wrapper">

			<div class="svg-header">

				<div class="mobile">
					<h1>News</h1>
				</div>
			</div>

		</div>
	</section>

	<section id="list">
		<div class="wrapper">



			<?php
				$args = array(
					'post_type' => 'news',
					'posts_per_page' => 50
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

		        <div class="news">
		        	<div class="image">
		        		<div class="content">
			        		<a href="<?php the_field('url'); ?>" rel="external">
								<img src="<?php $image = get_field('publication_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			        		</a>					        			
		        		</div>
		        	</div>

		        	<div class="info">
		        		<h3><a href="<?php the_field('url'); ?>" rel="external"><?php the_title(); ?></a></h3>
		        		<h4><?php the_time('F j, Y'); ?></h4>

		        		<a href="<?php the_field('url'); ?>" rel="external" class="read-more">Read the Article</a>
		        	</div>
		        </div>

			<?php endwhile; endif; wp_reset_postdata(); ?>
		

		</div>
	</section>
	
<?php get_footer(); ?>