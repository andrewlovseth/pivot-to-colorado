<?php get_header(); ?>

	<section id="hero">
		<div class="wrapper">
			
			<h1><?php the_field('404_headline', 'options'); ?></h1>
			<div class="deck">
				<?php the_field('404_copy', 'options'); ?>
			</div>

		</div>
	</section>

	   	
<?php get_footer(); ?>