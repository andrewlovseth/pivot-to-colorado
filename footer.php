	<footer>
		<div class="wrapper">

			<div class="social">

				<h5>Connect with us</h5>

				<div class="links">
					<?php if(have_rows('social_links', 'options')): while(have_rows('social_links', 'options')): the_row(); ?>
						<a href="<?php the_sub_field('link'); ?>" target="_blank">
							<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
					<?php endwhile; endif; ?>
				</div>

			</div>

			<div class="footer-container">
				<div class="info">
					<div class="text">
						<h2><?php the_field('footer_headline', 'options'); ?></h2>
						<?php the_field('footer_deck', 'options'); ?>
					</div>
					
					<div class="shape">
						<img src="<?php bloginfo('template_directory') ?>/images/footer-shape.png" alt="" />
					</div>
				</div>

				<div class="stay-updated-form">
					<?php $stay_updated_form = get_field('stay_updated_form', 'options'); echo do_shortcode($stay_updated_form); ?>
				</div>

			</div>

			<div class="copyright">

				<div class="links">
					<a href="/">Home</a>

					<?php if(have_rows('nav', 'options')): while(have_rows('nav', 'options')): the_row(); ?>
					 
					    <a href="<?php the_sub_field('link'); ?>" class="nav-<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>">
					        <?php the_sub_field('label'); ?>
					    </a>

					<?php endwhile; endif; ?>					
				</div>


				<div class="message">
					<?php the_field('copyright', 'options'); ?>
				</div>

			</div>

		</div>
	</footer>

	<?php get_template_part('partials/fact-overlay'); ?>
	
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

	<?php get_template_part('partials/google-analytics'); ?>

</body>
</html>