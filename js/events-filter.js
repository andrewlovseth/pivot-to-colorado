jQuery(function($) {

	//Load posts on page load
	events_get_posts();

	//Find Selected Categories
	function getSelectedType() {
		var type = $('.tabs a.active').attr('data-cat');	
		return type;
	}

	//On tab/filter change
	$('.tabs a').on('click', function(){
		events_get_posts();
	});


	// Fade in events 
	function fadeInEvents() {

		var events = $('body.post-type-archive-events #response .event');
		$(events).addClass('show');

		setTimeout(function() {
			if( $(events).hasClass('show') ) {
				$(events).addClass('fade-in');
			}
		}, 50);	
	}
	
	//Main ajax function
	function events_get_posts() {
		var ajax_url = ajax_listing_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'events_filter',
				type: getSelectedType,
			},
			beforeSend: function () {
				$('body.post-type-archive-events #response .event').removeClass('fade-in');
				//Show loader here
			},
			success: function(data) {
				//Hide loader here

				$('body.post-type-archive-events #response').html(data);
				fadeInEvents();
			},
			error: function() {
				$("body.post-type-archive-events #response").html('<p>There has been an error</p>');
			}
		});				
	}
	
});