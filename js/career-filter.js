jQuery(function($) {

	//Load posts on page load
	career_get_posts();

	//Find Selected Categories
	function getSelectedCategories() {
		var cat = $('.tabs a.active').attr('data-cat');	
		return cat;
	}

	function getSelectedCompanies() {
		var cat = $('.companies-dropdown a.active').attr('data-company');	
		return cat;
	}

	//On tab/filter change
	$('.tabs a, .companies-dropdown a').on('click', function(){
		career_get_posts();
	});
	
	//Main ajax function
	function career_get_posts() {
		var ajax_url = ajax_listing_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'career_filter',
				categories: getSelectedCategories,
				companies: getSelectedCompanies

			},
			beforeSend: function () {
				//Show loader here
			},
			success: function(data) {
				//Hide loader here
				$('body.post-type-archive-careers #response').html(data);


				$('.career .link a').on('click', function() {
					var careerLink = $(this).attr('href');
					var trackerName = ga.getAll()[0].get('name');

					ga(trackerName + '.send', 'event', {
						eventCategory: 'Outbound Link',
						eventAction: 'click',
						eventLabel: careerLink
					});
				});

			},
			error: function() {
				$("body.post-type-archive-careers #response").html('<p>There has been an error</p>');
			}
		});				
	}
	
});