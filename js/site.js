$(document).ready(function() {

	// 01 GLOBAL

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});
	

	// Menu Toggle
	$('#toggle').click(function(){
		$('header, nav').toggleClass('open');
		return false;
	});


	// FitVids
	$('.embed').fitVids();


	// Notch Smooth Scrool
	$('.notch a').on('click', function() {
		$.smoothScroll({
			scrollTarget: 'footer'
		});
		return false;
	});


	// Career Carousel
	$('#carousel').slick({
		dots: true,
		arrows: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		mobileFirst: true,
		adaptiveHeight: true
	});



	// Career Carousel
	$('#why-carousel').slick({
		dots: true,
		arrows: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		mobileFirst: true,
	});




	// Open Video
	$('#carousel .video .launch').on('click', function(){
		var video = $(this).closest('.video');
		var overlay = $(video).find('.overlay');

		$(overlay).addClass('open');

		return false;
	});


	// Close Video
	function closeVideo() {
		$('.overlay').removeClass('open');
	}

	$('.video .close').on('click', function(){
		closeVideo();

		return false;
	});


	// Tabs
	$('.tabs a').on('click', function(){
		$('.tabs a').removeClass('active');
		$(this).addClass('active')

		return false;
	});


	// Open Fact {
	$('.hotspot').on('click', function(){

		var headline = $(this).data('headline');
		var source = $(this).data('source');
		var factOverlay = ('.fact-overlay');
		var headlineSpot = $(factOverlay).find('h3');
		var sourceSpot = $(factOverlay).find('span.source');

		$(factOverlay).addClass('open');

		$(headlineSpot).text(headline);
		$(sourceSpot).text(source);

		return false;
	});


	// Close Fact
	function closeFact() {
		$('.fact-overlay').removeClass('open');
	}
	$('.fact-close').on('click', function(){
		closeFact();

		return false;
	});


	// Footer Form Floodlight Tags

    jQuery(document).bind('gform_confirmation_loaded', function(event, formId){
        if(formId == 1) {
        	callStaticFL('8503708', 'conve0', 'stayu0+standard')
        }
    });


	// 02 HOMEPAGE

	// Homepage Lifestyle Grid
	var $grid = $('.grid').masonry({
		itemSelector: '.grid-item',
		gutter: 20,
		percentPosition: true
	});

	$grid.imagesLoaded().progress( function() {
		$grid.masonry('layout');
	});

	$('section#lifestyle .cta .load-more').on('click', function(){
		$('section#lifestyle .grid .grid-item').show(100, function(){
			$grid.masonry('reloadItems');
			$grid.masonry('layout');
		});
		
		return false;
	});


	// Events on Home
	$('#featured-events').slick({
		dots: true,
		arrows: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		mobileFirst: true,
		adaptiveHeight: true
	});

	$('.line').on('inview', function(event, isInView) {
		if (isInView) {
			$(this).addClass('active');
		}
	});




	// 03 FEATURED CAREERS

	// Filters on Archive Careers
	$('.companies-dropdown .list a').on('click', function(){
		$('.companies-dropdown .list a').removeClass('active');
		$(this).addClass('active');
		var activeFilterText = $('.companies-dropdown .list a.active').text();
		$('.companies-dropdown .selected span').text(activeFilterText);
		$('.companies-dropdown .list, .companies-dropdown .selected span').toggleClass('open');
		return false;

	});

	// Active Filter
	var activeFilterText = $('.companies-dropdown .list a.active').text();
	$('.companies-dropdown .selected span').text(activeFilterText);

	$('.companies-dropdown .selected span').on('click', function(){
		$('.companies-dropdown .list').toggleClass('open');
		$(this).toggleClass('open');

		return false;
	});




	// 04 COMPANY PROFILE

	// Careers Link on Single Company
	$('.careers-link a').smoothScroll();



	$('body.single-companies .career a').on('click', function() {
		var careerLink = $(this).attr('href');
		var trackerName = ga.getAll()[0].get('name');

		ga(trackerName + '.send', 'event', {
			eventCategory: 'Outbound Link',
			eventAction: 'click',
			eventLabel: careerLink
		});
	});


	// 05 EVENTS

	// Show correct events header
	$('body.post-type-archive-events .tabs a').on('click', function(){
		var target = $(this).attr('data-cat');
		var header = $('#events-header').find('.'+target);

		$('#events-header .info').removeClass('active');
		$(header).addClass('active')

		return false;
	});

	$('section#meetup .cta .load-more').on('click', function(){
		$('section#meetup .meetup-event').show();
		$('section#meetup').addClass('opened');

		return false;
	});





	// 06 ABOUT

	// Facts Carousel on About
	$('#facts-carousel').slick({
		dots: true,
		arrows: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		mobileFirst: true,
	});



});


// Esc to close video modal
$(document).on('keyup',function(evt) {
    if (evt.keyCode == 27) {
		$('.overlay, .fact-overlay').removeClass('open');
    }
});




