<?php

/*

	Template Name: Home

*/

get_header(); ?>


	<?php if(get_field('google_tags')): ?>
		<?php the_field('google_tags'); ?>
	<?php endif; ?>

	<?php $facts = get_field('facts_carousel', 9); ?>

	<section id="hero">
		<div class="wrapper">

			<div class="banner">

				<h1 class="invisible"><?php echo get_bloginfo('name'); ?></h1>
				
				<div class="svg-header">
					<div class="desktop">
						<img src="<?php $image = get_field('svg_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="mobile">
						<img src="<?php $image = get_field('mobile_svg_header'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</div>

				<div class="about-statement">
					<?php the_field('about_statement'); ?>
				</div>

				<div class="cta">	
					<a href="<?php echo site_url('/careers'); ?>" class="btn">See Colorado Tech Jobs ></a>
				</div>
			</div>


		</div>
	</section>


	<section id="trees-divider">
		<div class="wrapper">	

		</div>
	</section>


	<section id="companies-list">
		<div class="wrapper">

			<div class="section-header">
				<h2><?php the_field('companies_headline'); ?></h2>
				<?php the_field('companies_deck'); ?>
			</div>

			<?php $posts = get_field('companies_list', 'options'); if( $posts ): ?>

				<div class="companies-list-wrapper">

				    <?php foreach( $posts as $post): setup_postdata($post); ?>

				    	<?php get_template_part('partials/company-minimal'); ?>

				    <?php endforeach; ?>

				</div>
			
			<?php wp_reset_postdata(); endif; ?>

			<div class="cta">

				<a href="<?php echo site_url('/companies/'); ?>" class="btn">Company Profiles ></a>

			</div>

		</div>
	</section>


	<section id="carousel-header">
		<div class="wrapper">

			<div class="section-header">
				<h2><?php the_field('careers_headline'); ?></h2>
				<?php the_field('careers_deck'); ?>
			</div>

		</div>
	</section>


	<?php get_template_part('partials/carousel'); ?>


	<section id="why-colorado">
		<div class="wrapper">

			<div class="section-header">
				<h2><?php the_field('why_colorado_headline'); ?></h2>
				<p><?php the_field('why_colorado_deck'); ?></p>
			</div>

			<div id="why-carousel">
				
				<?php if(have_rows('why_colorado_carousel')): while(have_rows('why_colorado_carousel')): the_row(); ?>
				 	<div class="slide">
					    <div class="stat">
					    	<div class="content">
						        <h4><?php the_sub_field('stat'); ?></h4>
					    	</div>
					    </div>

					    <div class="info">
					    	<h2><?php the_sub_field('headline'); ?></h2>
					    	<?php if(get_sub_field('sub_headline')): ?>
						    	<h3><?php the_sub_field('sub_headline'); ?></h3>
						    <?php endif; ?>
					    </div>				 		
				 	</div>

				<?php endwhile; endif; ?>

			</div>

			<div id="latest">
				
				<section id="upcoming-events">
					<h5>Upcoming Event:</h5>
					
					<?php $post_object = get_field('featured_event'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

				        <div class="event">
				        	<div class="image">
				        		<a href="<?php the_permalink(); ?>">
									<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				        		</a>
				        	</div>

				        	<div class="info">
				        		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				        		<h4><?php the_field('dates'); ?></h4>

				        		<a href="<?php the_permalink(); ?>" class="read-more">See Details</a>
				        	</div>
				        </div>

					<?php wp_reset_postdata(); endif; ?>

					<div class="cta">
						<a href="<?php echo site_url('/events/'); ?>" class="btn">All Events ></a>
					</div>

				</section>


				<section id="latest-news">
					<h5>Latest News:</h5>

					<?php
						$args = array(
							'post_type' => 'news',
							'posts_per_page' => 1
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					        <div class="news">
					        	<div class="image">
					        		<div class="content">
						        		<a href="<?php the_field('url'); ?>" rel="external">
											<img src="<?php $image = get_field('publication_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						        		</a>					        			
					        		</div>
					        	</div>

					        	<div class="info">
					        		<h3><a href="<?php the_field('url'); ?>" rel="external"><?php the_title(); ?></a></h3>

					        		<a href="<?php the_field('url'); ?>" rel="external" class="read-more">Read the Article</a>
					        	</div>
					        </div>

					<?php endwhile; endif; wp_reset_postdata(); ?>

					<div class="cta">
						<a href="<?php echo site_url('/news/'); ?>" class="btn">All News ></a>
					</div>

				</section>

			</div>

		</div>
	</section>

<?php get_footer(); ?>