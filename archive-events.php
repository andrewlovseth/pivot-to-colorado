<?php get_header(); ?>

	<?php if(get_field('events_google_tags', 'options')): ?>
		<?php the_field('events_google_tags', 'options'); ?>
	<?php endif; ?>

	<section id="hero">
		<div class="wrapper">

			<div class="svg-header">
				<div class="desktop">
					<img src="<?php $image = get_field('events_svg_header', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="mobile">
					<h1><?php the_field('events_headline', 'options'); ?></h1>
				</div>
			</div>

			<div class="deck">
				<?php the_field('events_deck', 'options'); ?>
			</div>

			<div class="tabs">
				<a href="#" data-cat="featured-local-events" class="active">Featured Local <br/>Events</a>
				<a href="#" data-cat="recruiting-and-events">Recruiting <br/>Events</a>
			</div>
		</div>
	</section>
	

	<section id="events-header">
		<div class="wrapper">
			
			<div class="info featured-local-events active">
				<h3><?php the_field('featured_local_events_headline', 'options'); ?></h3>
				<?php if(get_field('featured_local_events_deck', 'options')): ?>
					<p><?php the_field('featured_local_events_deck', 'options'); ?></p>
				<?php endif; ?>
			</div>

			<div class="info recruiting-and-events">
				<h3><?php the_field('recruiting_and_events_headline', 'options'); ?></h3>

				<?php if(get_field('recruiting_and_events_deck', 'options')): ?>
					<p><?php the_field('recruiting_and_events_deck', 'options'); ?></p>
				<?php endif; ?>
			</div>

		</div>
	</section>


	<section id="events">
		<div class="wrapper">

			<section id="response"></section>

		</div>
	</section>



	
<?php get_footer(); ?>