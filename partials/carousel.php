<?php if(have_rows('careers_carousel')): ?>
	
	 <section id="carousel">

	 	<?php while(have_rows('careers_carousel')) : the_row(); ?>
		    <?php if( get_row_layout() == 'profile' ): ?>
			  
			    <div class="slide profile">
					<div class="image">
						<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<?php $quote = get_sub_field('quote_group'); ?>
					<div class="quote">
						<div class="quote-wrapper">

							<blockquote>
								<p><?php echo $quote['quote']; ?></p>
							</blockquote>

							<div class="meta">
								<h4 class="name"><?php echo $quote['name']; ?></h4>
								<h5 class="title"><?php echo $quote['title']; ?></h5>
								<h6 class="company-name"><?php echo $quote['company']; ?></h6>
							</div>  

						</div>	
					</div>
				</div>
				
		    <?php endif; ?>
		 
		    <?php if( get_row_layout() == 'video' ): ?>
			  
			    <div class="slide video">

			    	<div class="poster">
			    		<a href="#" class="launch">
				    		<img src="<?php $image = get_sub_field('poster'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

							<div class="meta">
								<div class="wrapper">
									<h4 class="name"><?php the_sub_field('name'); ?></h4>
									<h5 class="title"><?php the_sub_field('title'); ?></h5>
									<h6 class="company-name"><?php the_sub_field('company'); ?></h6>
								</div>
							</div>  
				    	</a>
			    	</div>
					
					<div class="overlay">
						<a href="#" class="close">✖</a>

						<div class="overlay-wrapper">
							<div class="embed">
								<?php the_sub_field('video'); ?>
							</div>
						</div>
					</div>

				</div>
				
		    <?php endif; ?>

		<?php endwhile; ?>

	</section>

<?php endif; ?>