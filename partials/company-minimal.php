<div class="company-minimal <?php echo $post->post_name; ?>">
    <div class="content">
        <div class="logo">
            <a href="<?php the_permalink(); ?>">
                <img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </a>
        </div>    
    </div>
</div>