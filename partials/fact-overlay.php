<div class="fact-overlay">
	<div class="overlay-wrapper">

		<div class="content">
			<div class="fact-close">
				<a href="#">X</a>
			</div>

			<div class="info">
				<h4>
					<?php if(is_singular('companies')): ?>
						<div class="logo">
							<img src="<?php $image = get_field('logo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					<?php else: ?>
						<span class="headline">Denver is the</span>
					<?php endif; ?>

					<span class="arrow-line"></span>
					<span class="arrow"></span>
				</h4>
				<h3></h3>

				<span class="source"></span>
			</div>

			<div class="view-more">
				<a href="<?php echo site_url('/about/#facts'); ?>">View more</a>
			</div>
		</div>

	</div>
</div>