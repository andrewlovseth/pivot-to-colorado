<section id="share">
	<div class="wrapper">

		<h5>Share this with a friend</h5>
		
		<div class="links">

			<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>" class="linkedin" target="_blank">
				<img src="<?php echo bloginfo('template_directory'); ?>/images/share-linkedin.svg" alt="LinkedIn" />
			</a>
			
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>" class="facebook" target="_blank">
				<img src="<?php echo bloginfo('template_directory'); ?>/images/share-facebook.svg" alt="Facebook" />
			</a>

			<a href="https://twitter.com/intent/tweet/?text=<?php echo urlencode(get_the_title()); ?>&url=<?php echo urlencode(get_permalink()); ?>" class="twitter" target="_blank">
				<img src="<?php echo bloginfo('template_directory'); ?>/images/share-twitter.svg" alt="Twitter" />
			</a>

			<a href="mailto:?subject=Check out <?php echo urlencode(get_the_title()); ?>. They're hiring.&body=<?php echo urlencode(get_permalink()); ?>" class="email">
				<img src="<?php echo bloginfo('template_directory'); ?>/images/share-email.svg" alt="Email" />
			</a>

		</div>

	</div>
</section>