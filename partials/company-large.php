<div class="company company-large">
	<div class="bevel"></div>
	<div class="border"></div>
	<div class="color" style="border-color: <?php the_field('key_color'); ?>  transparent transparent transparent;"></div>

	<div class="logo">
    	<a href="<?php the_permalink(); ?>">
    		<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    	</a>
    </div>

    <div class="info">
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

        <div class="tagline">
        	<p><?php the_field('tagline'); ?></p>
        </div>

        <div class="description">
            <?php the_field('short_description'); ?>
        </div>
    </div>

    <div class="links">
        <div class="link">
        	<a href="<?php the_permalink(); ?>" class="arrow">View Company Details</a>
        </div>

        <div class="link">
            <a href="<?php the_permalink(); ?>#careers" class="arrow">View All Careers</a>
        </div>
    </div>

    <div class="bevel-bottom"></div>
    <div class="border-bottom"></div>
</div>