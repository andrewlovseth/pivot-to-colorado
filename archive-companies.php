<?php get_header(); ?>

	<?php if(get_field('companies_google_tags', 'options')): ?>
		<?php the_field('companies_google_tags', 'options'); ?>
	<?php endif; ?>

	<section id="hero">
		<div class="wrapper">

			<div class="svg-header">
				<div class="desktop">
					<img src="<?php $image = get_field('companies_svg_header', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="mobile">
					<h1><?php the_field('company_profiles_headline', 'options'); ?></h1>
				</div>
			</div>

			<div class="deck">
				<?php the_field('company_profiles_deck', 'options'); ?>
			</div>

		</div>
	</section>

	<section id="list">
		<div class="wrapper">
		
			<?php $posts = get_field('companies_list', 'options'); if( $posts ): ?>

				<div class="companies-list-wrapper">

				    <?php foreach( $posts as $post): setup_postdata($post); ?>

				    	<?php get_template_part('partials/company-large'); ?>

				    <?php endforeach; ?>

				</div>
			
			<?php wp_reset_postdata(); endif; ?>

		</div>
	</section>
	
<?php get_footer(); ?>
